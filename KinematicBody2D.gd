extends KinematicBody2D

const SPEED = 200

var motion = Vector2()

enum CHARACTER_HEADING { RIGHT, UP, DOWN }
var character_heading

func _physics_process(delta):
    if Input.is_action_pressed("ui_up"):
        motion.y = -SPEED
        get_node("Sprite").get_node("AnimationPlayer").play("Up")
        character_heading = CHARACTER_HEADING.UP
    elif Input.is_action_pressed("ui_down"):
        motion.y = SPEED
        get_node("Sprite").get_node("AnimationPlayer").play("Down")
        character_heading = CHARACTER_HEADING.DOWN
    elif Input.is_action_pressed("ui_left"):
        motion.x = -SPEED
        get_node("Sprite").flip_h = true
        get_node("Sprite").get_node("AnimationPlayer").play("Right")
        character_heading = CHARACTER_HEADING.RIGHT
    elif Input.is_action_pressed("ui_right"):
        motion.x = SPEED
        get_node("Sprite").flip_h = false
        get_node("Sprite").get_node("AnimationPlayer").play("Right")
        character_heading = CHARACTER_HEADING.RIGHT
    else:
        motion.x = 0
        motion.y = 0
        match character_heading:
            CHARACTER_HEADING.UP:
                get_node("Sprite").get_node("AnimationPlayer").play("Idle Up")
            CHARACTER_HEADING.DOWN:
                get_node("Sprite").get_node("AnimationPlayer").play("Idle Down")
            CHARACTER_HEADING.RIGHT:
                get_node("Sprite").get_node("AnimationPlayer").play("Idle Right")
    motion = move_and_slide(motion)